<?php
declare(strict_types=1);

class Operator
{
    private $operandOne;
    private $operandTwo;

    public function __construct($operandOne, $operandTwo)
    {
        $this->operandOne = $operandOne;
        $this->operandTwo = $operandTwo;

    }

    public function add(): int {
        return 0;
    }
    public function substract(): int {
        return 0;
    }
    public function multiply(): int {
        return 0;
    }
    public function divide(): int {
        return 0;
    }

}
