<?php
require_once 'model/Operator.php';
$numberOne = 4;
$numberTwo = 2;
$operator = new Operator($numberOne, $numberTwo);
echo($numberOne . ' + '. $numberTwo . ' = ' . $operator->add() . '<br/>');
echo($numberOne . ' - '. $numberTwo . ' = ' . $operator->substract() . '<br/>');
echo($numberOne . ' * '. $numberTwo . ' = ' . $operator->multiply() . '<br/>');
echo($numberOne . ' / '. $numberTwo . ' = ' . $operator->divide() . '<br/>');

