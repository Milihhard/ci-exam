pipeline {
  agent any
  options{
    buildDiscarder(logRotator(artifactNumToKeepStr: '5', numToKeepStr: '5'))
  }
  triggers {
    pollSCM 'H/15 7-18 * * *'
  }
  stages {
    stage('Init') {
      steps {
        script {
          if (isUnix()) {
            sh "rm -rf vendor 2>nul"
          } else {
            bat 'if exist vendor rmdir /s /q vendor'
          }
        }
      }
    }
    stage('Build') {
      steps {
        script {
          if (isUnix()) {
            sh "composer install"
          } else {
            bat 'composer install'
          }
        }
      }
    }
    stage('Tests') {
      parallel {
        stage('Unit tests') {
          steps {
            script {
              if (isUnix()) {
                sh "vendor/bin/phpunit"
              } else {
                bat 'vendor/bin/phpunit'
              }
            }
          }
          post {
            always {
              publishHTML(
                [allowMissing: true,
                  alwaysLinkToLastBuild: true,
                  keepAll: true,
                  reportDir: 'reports/html',
                  reportFiles: 'index.html',
                  reportName: 'HTML Report', reportTitles: 'Report'
                ]
              )
              junit 'reports/junit.xml'
            }
          }
        }
        stage('Checkstyle') {
          steps {
            script {
              if (isUnix()) {
                sh 'vendor/bin/phpcs -q --report=junit --report-file=reports/phpcs-junit.xml'
              } else {
                bat 'vendor/bin/phpcs -q --report=junit --report-file=reports/phpcs-junit.xml'
              }
            }
          }
          post {
            always {
                junit 'reports/phpcs-junit.xml'
            }
          }
        }
      }
    }
    stage('Archiving') {
      parallel {
        stage('Src Archive') {
          steps {
            archiveArtifacts artifacts: 'src/**', fingerprint: true, onlyIfSuccessful: true
          }
        }
        stage('PHP Doc') {
          steps {
            script {
              if (isUnix()) {
                sh 'php phpDocumentor.phar'
              } else {
                bat 'php phpDocumentor.phar'
              }
            }
          }
          post{
            success {
              publishHTML(
                [allowMissing: true,
                  alwaysLinkToLastBuild: true,
                  keepAll: true,
                  reportDir: 'reports/documentation',
                  reportFiles: 'index.html',
                  reportName: 'PHP Doc', reportTitles: 'PHP Doc'
                ]
              )
            }
          }
        }
      }
    }
  }
}
