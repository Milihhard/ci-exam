# Continuous integration exam

In this project you have to create an Operator class and implements test.

## Usage

Operator object is construct with two number.
It has 4 methods add, substract, multiply and divide. I want to use the class like this :

```php
$operator = new Operator(2,3);
echo($operator->add() . '<br/>');
echo($operator->substract() . '<br/>');
echo($operator->multiply() . '<br/>');
echo($operator->divide() . '<br/>');
```


## Constraint

- For the purpose of the exam, a code coverage of 100% is needed
- The pipeline must be in a successful state
- You cannot change the pipeline